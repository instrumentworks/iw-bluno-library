#include <Bluno.h>

// declare version number here
int versionNumber = 100;

// define the range of set points
double spMax = 100.0;
double spMin = 0;

// declare if custom commands are in use
bool custom = false;

Bluno bluno(verionNumber, spMax, spMin, custom);

bool newSetPoint(double sp) {
  // implement code on putting a new set point
  // return TRUE if successful, FALSE if failed

}

bool calibrate(String command) {
  // implement calibration code here
  // return TRUE if successful, FALSE if failed

}

bool stopDevice() {
  // implement code to stop the device
  // return TRUE if successful, FALSE if failed

}

double currentSetPoint() {
  // implement code to return the current set point

}

void customCommand(String command) {
  // implement code to handle custom commands

}

void setup() {
  // set up with default baud rate of 115200
  // change baud rate if necessary
  bluno.Setup(115200);

}

void loop() {
  bluno.ReadCommand();
}