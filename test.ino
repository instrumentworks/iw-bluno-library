#include <Bluno.h>

// initialise version number and set point range
int verNumber = 100; 
double spMax = 100.0;
double spMin = 0.0;

// declare if custom commands are in use
bool custom = true;

Bluno bluno(verNumber,spMax,spMin,custom);

double current_sp;

bool ss = false;

bool newSetPoint(double sp){
  //place a new set point

  if (sp < 70){
  current_sp = sp;
  return true;
  } else {
    return false;
  }
}
double currentSetPoint(){
  //return the current set point
  return current_sp;
}

bool stopDevice(){
  ss = !ss;
  return ss;
}
bool calibrate(String command){
  if (command.charAt(1)=='1'){
    return true;
  } else {
    return false;  
  }
}
bool customCommand(String command){
  if (command.charAt(0)=='A'){
    bluno.ReturnCommand("A - pass");
    return true;
  } else if (command.charAt(0)=='B'){
    if (command.charAt(1)=='1'){
      bluno.ReturnCommand("B - pass");
      return true;
    } else {
      bluno.ReturnCommand("B - failed");
      return true;
    }
  } else if (command.charAt(0)=='D'){
    if (command.charAt(1)=='1'){
      bluno.WriteData('k');
    } else if (command.charAt(1)=='2'){
      bluno.WriteData(1.23456);
    } 
    return true;
  } else if (command.charAt(0)=='E'){
    if (command.charAt(1)=='1'){
      bluno.ReturnCommand("k234567890");
    } else if (command.charAt(1)=='2'){
      bluno.ReturnCommand("k2345678901234567890");
    } else if (command.charAt(1)=='3'){
      bluno.ReturnCommand("k2345678901234567890123456789012345678901234567890");
    } else if (command.charAt(1)=='4'){
      bluno.ReturnCommand("56");
    }
    
    return true;
  } else {
    return false;  
  }
  
}


void setup() {
  bluno.Setup(115200);
}

void loop() {
  // default action is to read command on a loop
  bluno.ReadCommand();
  // add code if necessary
}