#Bluno Library#

This repository contains codes for the Bluno Arduino Library. The library defines the Bluno C++ Class which provides several functions to handle communication with the DataWorks app. A number of functions are declared and they are to be implemented in the INO by the user. 

This library is compatible with all the DFRobot Bluno microcontrollers.
https://www.dfrobot.com/index.php?route=product/category&path=35_104


##Functions provided by the Bluno Class##
---
`Bluno::Bluno(int version, double spMax, double spMin, bool custom)`

Use this function when creating an instance of the Bluno Class. It requires four parameters from the INO and they are stored respectively as private variables. `int version` provides the version number of the INO, `double spMax` and `double spMin` define the range of set points, and `bool custom` declares whether custom commands are in use.

---
`void Bluno::Setup(long BaudRate)`

Using this function to set up the serial communication with the Bluno unit using the specified baud rate. This function will typically be placed in the `void setup()` loop of the INO. The default baud rate should be 115200 but other rates can be specified if necessary.

---
`void Bluno::WriteData(double value)`

Use this function to send a numerical value for displaying on DataWorks as data. The function parameter is declared as a `double` type but other numerical types such as `int` can be also be used. Note that, while attempting to pass a `String` to this function will cause compilation error, passing a `char` type to this function will not cause compilation problems and this function sends the ASCII value of the character to DataWorks (This is a minor bug which we may look into fixing in the future).

---
`void Bluno::ReturnCommand(String command)`

Use this function to send messages to display on the command line in DataWorks. Note that DataWorks has a limit of displaying only 20 characters.

---
`void Bluno::PrintError()`

Use this function if an unknown command is received to display an error message.

---
`double Bluno::LibVersion()`

Use this function to obtain the current version number of this Bluno Library.

---
`void ReadCommand()`

Use this function to handle commands sent from DataWorks to the Bluno unit. A list of default commands are available (see below). Non-default commands are handled by `CustomCommand` if they are in use or `PrintError()` will be called if received command is not recognised.

---

##Default Commands##
|Command|Description|
|---|---|
|S##.##|Call `bool newSetPoint(double sp)` to create a new set point using the number following 'S'.|
|S?|Call `double currentSetPoint()` to display the current set point.|
|SR|Display the accepted range of set points defined when creating the instance of the Bluno Class.|
|SS|Call `bool stopDevice()` to stop the device.|
|V|Display the version number of the INO.|
|C|Call `bool calibrate(String command)` to perform calibration. This command can be appended with additional commands to specify how the calibration is done, depending on the implementation of `bool calibrate(String command)`.|
|Non-default commands|If custom commands are in use, `bool customCommand(String command)` will be called. Otherwise `PrintError()` will called to display an error message.|

##User-implemented Functions##

The following functions are declared and used by the Bluno Library. However, the definitions of the functions are to be implemented by the user in the INO.

---
`bool newSetPoint(double sp)`

This function requires code to place a new set point at the value of `double sp`. After placing the set point, this function should either return TRUE if the new set point is successfully placed or FALSE if it failed.

---
`double currentSetPoint()`

This function requires code to return the value of the current set point.

---
`bool stopDevice()`

This function requires code to stop the device and return TRUE/FALSE reflecting whether the device was successfully stopped.

---
`bool calibrate(String command)`

This function requires code to perform calibration. The command sent from DataWorks is passed into this function as `String command` (beginning with the letter 'C') and the user can choose how they would handle the command and how the calibration is to be done. After the calibration, this function should return TRUE/FALSE reflecting if it was successful.

---
`bool customCommand(String command)`

This function requires code to handle any non-default commands that the user wishes to use. The command sent from DataWorks is passed into this function as `String command` and the user can choose how the commands are handled. This function should return TRUE after handling the command to indicate that a custom command has been handled, or return FALSE if the received command does not match with the user-defined commands.

Note that user-defined commands should not overlap with existing default commands. In the case they do, this library will prioritise default commands and ignore the overlapping user-defined commands.

If the user does not need to use any non-default commands, this function can be left blank.

---


##Template##

`template.ino` is provided for the standard use of this Bluno Library.