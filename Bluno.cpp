#include "Arduino.h"
#include "Bluno.h"
#define MSG_LENGTH 18
#define LIB_VER 0.10

boolean checkNumber (String message)
{
  boolean isNum=false;
  for(byte i=0;i<message.length();i++){
    isNum = isDigit(message.charAt(i)) || message.charAt(i) == '+' || message.charAt(i) == '.' || message.charAt(i) == '-';
    if(!isNum){ 
      return false;
    }
  }
  return isNum;
}



Bluno::Bluno(int version, double spMax, double spMin, bool custom)
{
  _version = version;
  _spMax = spMax;
  _spMin = spMin;
  _custom = custom;
}

void Bluno::Setup(long BaudRate)
{
    Serial.begin(BaudRate);
    while(!Serial)
    {
    ;
    }
}

void Bluno::WriteData(double value)
{
	Serial.println(value);
	
}

void Bluno::ReturnCommand(String command)
{

		Serial.println(command);
}

void Bluno::PrintError()
{
	Serial.println("E: Unknown Command");
}

double Bluno::LibVersion()
{
	return LIB_VER;
}

void Bluno::ReadCommand()
{
	String command;
	double number;
	while (Serial.available())
	{
		if (Serial.peek()=='S'){
			command = Serial.readString();
			
			if (command.charAt(1)=='?'){
				Serial.print("Setpoint: ");
				Bluno::WriteData(currentSetPoint());
			} else if (command.charAt(1)=='R'){
				Serial.print("Range:");
				Serial.print(_spMin);
				Serial.print("-");
				Serial.println(_spMax);
				
			} else if (command.charAt(1)=='S'){
				if (stopDevice()){
					Serial.println("SP - devicestopped");
				} else {
					Serial.println("SP - stop invalid");
				}
			}  else {
				command.remove(0,1);
				if (checkNumber(command)){
					number = command.toFloat();
					if (number < _spMin || number > _spMax){
						Serial.println("SP - out of range");
					} else {
						if (newSetPoint(number)){
							Serial.println("SP - accepted");
						} else {
							Serial.println("SP - error");
						}	
					}
				} else {
					Serial.println("SP - not number");
				}
			}
			
		} else if (Serial.peek()=='V'){
			Serial.readString();
			Serial.print("Version:");
			Serial.println(_version);
		} else if (Serial.peek()=='C'){
			command = Serial.readString();
			if (calibrate(command)){
				Serial.println("C - success");
			} else {
				Serial.println("C - failed");
			}
		} else {
			if (_custom){
				command = Serial.readString();
				bool response = customCommand(command);
				if (response){
				} else {
					Bluno::PrintError();
				}
				
			} else {
 				Bluno::PrintError();
			}
		}
				
	}
	
}