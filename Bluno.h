#ifndef Bluno_h
#define Bluno_h

#include "Arduino.h"

// list of functions to be implemented by user
// Boolean functions should return TRUE if successful, FALSE if failed
bool newSetPoint(double sp); 

bool calibrate(String command);

bool stopDevice();

double currentSetPoint();

bool customCommand(String command);
// use this function if user-defined commands are needed, set custom = TRUE when initialising the Bluno class
// otherwise leave blank and set custom =  FALSE on initialisation
// when defining custom commands, avoid overlapping with existing default commands

// end of list

class Bluno
{

	
	public:
	Bluno(int version, double spMax, double spMin, bool custom);
	// load the necessary information when initialising the Bluno class
	// include version number, range of set points spMin and spMax
	// custom = TRUE if user-defined commands are to be used, FALSE otherwise
	
	void Setup(long BaudRate);
	// setup the BLE communication
	
	void WriteData(double value);
	// write a data value to the app
	
	void ReturnCommand(String command);
	// display a command line on the app, restricted to 20 characters in length
	
	void ReadCommand(); 
	// read the command sent from the app and perform the appropriate action
	// default commands:
		// SABC.XYZ : place new set point at ABC.XYZ, where ABC.XYZ is a float number
		// SR: display the valid range of set points
		// S?: display the current set point
		// SS: stop the device
		// V: display version number
		// C: perform calibration
	
	
	void PrintError(); 
	// call this function if an unknown command is entered
	
	double LibVersion();
	
	private:
	int _libVersion;
	int _version;
	double _spMax;
	double _spMin;
	bool _custom;
};

#endif